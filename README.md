## Welcome to the redis library.
The intention of this library is to provide a fully functional bash interfact to a redis database. Most functions should work correctly.

### How do I use the library.

#### Load it!
```
. ./redis
```

#### Create a redis socket connection!
```
redis_socket socketname server port
```

#### Set the activedb!
```
activedb socketname
```

#### Use redis commands!
```
$ redis ping
PONG
$ redis set test "Setting a test string!"
OK
$ redis get test
Setting a test string!
```

### Redis library unit-tests and example access script.

The unit tests are available for viewing and addition to in the [redis-tests](https://gitlab.com/linuxclusterfsck/modules/redis-tests/) repository.

This contains [output](https://gitlab.com/linuxclusterfsck/modules/redis-tests/blob/master/tests-output) data of all the tests.

#### Incase data gets stuck on a command output.
The buffer from the server will need to be flushed, You can do this with a `cat <&$activedb` command. If this happens please produce a issue report and if you want a pull request with a patch.

### Some things are untested currently.

I've not tested *everything* so if you find any issues please report.
