#!/usr/bin/env bash

if [[ $(type -t cleanup_redis) ]]
  ## Unsets lib-build before defining a new version if it is reloaded.
  then
  cleanup_redis
fi

redis_socket()
## This creates a native bash socket.
## The first argument will be used as the name of the socket.
## The second argument will be used as the server.
## The third argument will be used as the port.
{
  if [[ "$#" -eq 3 ]]
  then
    local socketname server port
    socketname="$1" ## Sets a socketname.
    server="$2" ## Sets the server.
    port="$3" ## Sets the port.
    ## We could have just used $1, $2 and $3 in the socket creation
    ## But creating these variables makes the code more readable.
    #printf '\n%s %s %s %s:%s\n' "Connecting to socket" "$socketname" "@" "$server" "$port"
    eval "exec {$socketname}<>/dev/tcp/$server/$port" # Initiates the connection.
  else ## On failure tell people how to create the socket.
    printf '%s\n' "Usage: redis_socket name server port"
    return 1
  fi
}
redisfunctions+=("setup_socket")
redisvariables+=("socketname" "server")

close_socket()
## Closes a socket.
## Requires a socket name.
{
  if [[ "$#" -eq 1 ]]
  ## If the function was passed 1 argument.
  then
    if [[ $(type -t redis) ]]
    then
      redis quit &>/dev/null
    fi
    socketname="$1" ## Set the socketname to the first argument.
    eval "exec {$socketname}>&-" ## Closes socket output.
    ret="$?" # Determines output of function.
    eval "exec {$socketname}<&-" ## Closes socket input.
    ret+="$?"
  else
    ret=1
  fi
  if [[ "$ret" -eq 0 ]]
  ## If the correct number of arguments and the sockets were closed print a success.
  then
    printf '%s\n' "Redis database socket $socketname closed."
  else
    ## The right number of arguments were not passed so we are going to tell the user.
    printf '%s\n' "Requires a socketname!"
  fi
}
redisfunctions+=("closesocket")
redisvariables+=("ret")

activedb()
## Sets the active database socket to use.
{
  typeset -ig activedb="$1"
  # printf '%s %s %s\n' "Redis active database initialized to" "$1" "socket."
}
redisfunctions+=("activedb")
redisvariables+=("activedb")

redis_read_str()
## Creates a redis string to output.
{
  #typeset redis_str="$*"  ## Sets redis_str to all input taken by the function.
  redis_str="${*%$'\n'}" ## sets redis_str and trims newline.
  redis_str="${redis_str%$'\r'}" ## Removes a return carriage.
  redis_str+=$'\n' ## Adds the newline back it was needed.
  printf '%b' "$redis_str" ## Print to output.
}
redisfunctions+=("redis_read_str")
redisvariables+=("redis_str")

redis_read_err()
{
  #typeset redis_err="$*" ## Sets redis_err to all input taken by the function.
  redis_err="${*%$'\n'}" ## Removes a newline.
  redis_err="${redis_err%$'\r'}" ## Removes a return carriage.
  redis_err+=$'\n' ## Adds the newline back in.
  printf '%s' "$redis_err" ## Print to output.
  return 1 ## Changes return status code to reflect an error.
}
redisfunctions+=("redis_read_err")
redisvariables+=("redis_err")

redis_read_int()
## Parses a redis int.
## Takes one argument.
{
  #redis_int="$1" # Sets the int to the first argument sent to the function.
  redis_int="${1%$'\n'}" # Sets redis_int and trims newline.
  typeset -i redis_int="${redis_int%$'\r'}" # Removes the return carriage.
  printf '%b\n' "$redis_int" ## Print to output.
}
redisfunctions+=("redis_read_int")
redisvariables+=("redis_int")


redis_read_bulk()
{
  redis_bulk="$*"
  raw_len="${#redis_bulk}"
  redis_bulk="${redis_bulk%$'\n'}" ## This sets the redis_bulk and trims newline.
  redis_bulk="${redis_bulk%$'\r'}" # Removes the return carriage.
  typeset -i str_len="${#redis_bulk}" ## Check how long the string is.
  if [[ "$bytecount" = "$str_len" ]] && [[ -z "$noeq" ]]
  ## Compare the string with the expected value.
  then
    redis_bulk+=$'\n' # Add back the newline.
    printf '%s' "$redis_bulk" ## Print to output.
  elif [[ "$bytecount" -gt "$raw_len" ]]
  ## This allows us to cycle through a very large bulk string with no array.
  then
    noeq="1" # noeq makes sure we don't change from a noeq to an eq state.
    redis_bulk+=$'\n' # add the newline.
    printf '%s' "$redis_bulk" # Print it.
    ((bytecount=bytecount-("$raw_len"+1))) # The +1 is for newlines.
    ((param_cur-=1)) # Allow us to keep cycling through.

    if [[ "$bytecount" -le 0 ]]
    # cause the break out when we're out of things to read.
    then
      param_cur=1 # param_count will only ever be 1, Thus this causes a breakout
    fi
  fi
}
redisfunctions+=("redis_read_bulk")
redisvariables+=("redis_bulk" "str_len" "noeq")

redis_read()
## Reads input from the activedb socket.
{
  if [[ "$#" -eq 0 ]]
  ## If there are no arguments passed then this is the first iteration and we should ensure we don't have residual data.
  then
    unset -v param_count param_cur ## Unsets the counter variables we use in the iterations.
  fi
  if [[ "$#" -eq  1 ]]
  ## If we have sent a single argument then set param_count to it.
  then
    typeset -i param_count="$1"
    typeset -i param_cur=1 ## Also start with a baseline cursor position of 1.
  fi
  while read -r socket_data # Reads the socket data line by line.
  do
    typeset first_char="${socket_data:0:1}" ## Sets the first character.
    case "$first_char" in ## Checks the first character.
      '+')
          socket_data="${socket_data#+}" ## Removes the leading + symbol.
          if [[ -z "$BULKONLY" ]]
          ## If the -b flag has not been set we will print this data.
          then
            redis_read_str "$socket_data" ## Prints the string.
          fi
          ;;
      '-')
          socket_data="${socket_data#-}" ## Removes the leading - symbol.
          redis_read_err "$socket_data" # Prints the error data.
          typeset -i ret="$?" ## Tells the shell to return what redis_read_err returned.
          ;;
      ':')
          socket_data="${socket_data#:}" ## Removes the leading : symbol.
          if [[ -z "$BULKONLY" ]]
          ## If the -b flag has not been set we will print this data.
          then
            redis_read_int "$socket_data" # Prints the integer.
          fi
          ;;
      '$')
          bytecount="${socket_data#$}" ## Removes the leading $ symbol.
          bytecount="${bytecount%$'\n'}" ## Trims the newline.
          bytecount="${bytecount%$'\r'}" ## Trims the carriage return.
          if [[ "$bytecount" -lt 0 ]]
          ## A bytecount of less than 0 represents a null bulk string
          ## Protocol states we output some form of null.
          then
            printf '%s\n' "null" # Output the null.
            typeset -i ret=1 # Set our return status.
          fi
          if [[ -z "$param_count" ]] && [[ "$bytecount" -gt 0 ]]
          # If we've recieved a bulk string not contained in an array
          # Assume we've got one parameter sent to us to print.
          then
            param_count=1 # If we have a bytecount greater than 0 and we're on the first iteration. At least give us a starting param_count of 1.
          fi
          if [[ ! -z "$param_cur" ]] && [[ "$bytecount" -ge 0 ]]
          ## If we recieve a bulk string, This cancels out the addition in the return statement iteration, Allowing us to read the data.
          then
            ((param_cur-=1)) ## We decrement here because a bulk string has 2 \r\n sequences.
            ## One for how many characters in the request. Another for data.
          fi
          ;;
      '*')
          paramcount="${socket_data#\*}" ## Remove the leading * character.
          paramcount="${paramcount%$'\n'}" # Trim the newline.
          paramcount="${paramcount%$'\r'}" # Trim the carriage return.
          if [[ "$paramcount" -gt 0 ]]
          then
            redis_read "$paramcount" # Start a new iteration with a count of the array.
          fi
          ;;
        *)
          # Read the bulk data from the $) iteration.
          redis_read_bulk "$socket_data"
          ;;
    esac
    #echo "$param_count $param_cur"
    if [[ ! -z "$param_count" ]]
    # If the parameter count is set.
    then
      if [[ "$param_cur" -lt "$param_count" ]]
      # If the cursor is less than the paramater count.
      then
        ((param_cur+=1)) # Increment the parameter count.
        continue # Continue the next iteration.
      else # This will happen if param_cur equals the count.
        unset -v noeq
        break
      fi
    else # This will happen if param_count does not exist.
      break
    fi
  done<&"$activedb" ## Send the activedb to the loop.

  if [[ -z "$ret" ]]
  ## If there is no return status.
  then
    ret=0 ## We will return 0.
  fi
  return "$ret" ## Return the status.
}
redisfunctions+=("redis_read")
redisvariables+=("param_count" "param_cur" "first_char" "bytecount" "paramcount" "ret")

redis()
## Creates a resp string and sends it to the activedb.
## Automatically provides output through redis_read
{
  case "$1" in
    ## Set the BULKONLY variable to be sent to redis_read.
    '-b') ## This allows bulkonly output useful for getting a command list.
    local BULKONLY="YES"
    shift ## Shifts the "$@" variable so -b no longer exists there.
    ;;
  esac

  respdata=("$@") ## Grab all input sent to the function.
  resp_string="*${#respdata[@]}\r\n" ## Sets the array value to the number of array entries sent to the function.
  for (( i=0 ; i<"${#respdata[@]}" ; i++ ))
  ## Iterate through respdata while using a C++ style for loop.
  ## We do this instead of a for entry in "${array[@]}" style because it allows us to use the iteration data more creatively.
  do
    resp_string+="\$${#respdata[$i]}\r\n${respdata[$i]}\r\n"
    ## Create a bulk string containing a bytecount of the length of the entry
    ## and the data.
  done
  printf %b "$resp_string" >&$activedb ## Send the string to the activedb
  BULKONLY="$BULKONLY" redis_read ## Read the response from the server.
  return "$?" ## Forward any return codes passed by redis_read.
}
redisfunctions+=("redis")
redisvariables+=("respdata" "resp_string")

cleanup_redis()
## Clean up everything to do with the redis module.
{
  ## Unset the functions.
  redisfunctions+=("cleanup_redis")
  for function in "${redisfunctions[@]}"
  do
    unset -f "$function"
  done
  ## Unset the variables. "_" contains us running cleanup_redis
  ## We clean up so the terminal is exactly the same as if you had
  ## opened a new one.
  redisvariables+=("_" "var")
  for var in "${redisvariables[@]}"
  do
    unset -v "$var"
  done
}
